# Attributed graph embedding

Source code for master thesis "Attributed Graph Embedding".
Implementation is based on source code for https://arxiv.org/pdf/1809.05124.pdf

Repository contains:
 - preprocessing of Enron email dataset (starting from raw emails)
 - outcome dataset
 - implementation of proposed edge encoding methods
 - implementation for proposed models
 - evaluation code for node classification and link prediction task
 