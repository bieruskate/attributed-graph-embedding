"""
Data preprocessing functions
"""
import numpy as np
import sklearn.utils as sk_utils


def create_link_prediction_dataset(graph, split_proportion, nb_edge_labels=0):
    edge_list = _sort_edges_by_time(graph)
    pos_train, pos_dev, embedding_train = _split_dataset(edge_list,
                                                         split_proportion,
                                                         nb_edge_labels)

    neg_train = _generate_negative_samples(graph, len(pos_train))
    neg_dev = _generate_negative_samples(graph, len(pos_dev))

    x_train, y_train, x_dev, y_dev = _prepare_dataset(pos_train, neg_train,
                                                      pos_dev, neg_dev)

    return embedding_train, x_train, y_train, x_dev, y_dev


def _sort_edges_by_time(nx_graph):
    edge_list = list(nx_graph.edges(data=True))
    sorted_list = sorted(edge_list, key=lambda edge: edge[2]['timestamp'])

    return sorted_list


def _split_dataset(edge_list, train_dev_split, nb_edge_labels):
    items_count = len(edge_list)
    train_items_count = int(items_count * train_dev_split)

    print(f'''
    Split statistics:
    Items in Dataset: {items_count}
    Split proportions: {train_dev_split}
    Items in Train dataset: {train_items_count}
    Items in Test dataset: {items_count - train_items_count}
    ''')

    train_dataset = edge_list[:train_items_count]
    dev_dataset = edge_list[train_items_count:]

    embedding_train_dataset = []

    for (node_u, node_v, data) in train_dataset:
        edge_data = [node_u, node_v, data['timestamp']]

        for label in map(str, np.arange(nb_edge_labels)):
            edge_data.append(data[label])

        embedding_train_dataset.append(edge_data)

    train_dataset = [(edge[0], edge[1]) for edge in train_dataset]
    dev_dataset = [(edge[0], edge[1]) for edge in dev_dataset]
    embedding_train_dataset = [list(et) for et in embedding_train_dataset]

    return train_dataset, dev_dataset, embedding_train_dataset


def _generate_negative_samples(graph, samples_count):
    neg_samples = []

    for _ in range(samples_count):
        init_node = np.random.choice(graph.nodes)
        second_node_list = list(graph.nodes())
        for edge in set(graph.edges(init_node)):
            second_node_list.remove(edge[1])

        second_node = np.random.choice(second_node_list)
        neg_samples.append((init_node, second_node))

    return neg_samples


def _prepare_dataset(pos_train, neg_train, pos_dev, neg_dev):
    y_pos_train = np.ones((len(pos_train), 1), dtype=int)
    y_neg_train = np.zeros((len(neg_train), 1), dtype=int)

    y_pos_dev = np.ones((len(pos_dev), 1), dtype=int)
    y_neg_dev = np.zeros((len(neg_dev), 1), dtype=int)

    x_train = np.concatenate((np.array(pos_train), np.array(neg_train)))
    y_train = np.concatenate((y_pos_train, y_neg_train))

    x_dev = np.concatenate((np.array(pos_dev), np.array(neg_dev)))
    y_dev = np.concatenate((y_pos_dev, y_neg_dev))

    x_train, y_train = sk_utils.shuffle(x_train, y_train, random_state=0)
    x_dev, y_dev = sk_utils.shuffle(x_dev, y_dev, random_state=0)

    return x_train, y_train, x_dev, y_dev
