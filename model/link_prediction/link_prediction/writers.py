"""
Writers functions
"""
import os


def export_dataset(processed_dataset, output_dir):

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    filenames = [
        'embedding_train.txt',
        'train.txt', 'y_train.txt',
        'dev.txt', 'y_dev.txt'
    ]

    for filename, data in zip(filenames, processed_dataset):
        with open(os.path.join(output_dir, filename), 'w+') as f:
            for item in data:
                line = ' '.join(map(str, item))
                f.write(line + '\n')
