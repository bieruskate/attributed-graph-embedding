"""
Metrics functions
"""
import numpy as np
import sklearn.metrics as sk_metrics


def prediction_report(y_true, y_pred):
    metrics = sk_metrics.precision_recall_fscore_support(y_true, y_pred)
    accuracy = sk_metrics.accuracy_score(y_true, y_pred)
    cm = sk_metrics.confusion_matrix(y_true, y_pred)

    accuracy_per_class = np.array([row[i] / np.sum(row)
                                   for i, row in enumerate(cm)])

    report = dict()
    report['precision'] = metrics[0]
    report['recall'] = metrics[1]
    report['f1-score'] = metrics[2]
    report['support'] = metrics[3]
    report['accuracy'] = accuracy_per_class

    return report, accuracy
