"""
Edge embeddings
"""
import numpy as np


def get_unknown_node_embedding(dim):
    return np.ones(dim)


def get_node_vector(model, node, unknown_nodes=True):
    if not unknown_nodes or str(node) in model.wv.vocab:
        return model.wv.get_vector(str(node))

    return get_unknown_node_embedding(128)


def hadamard_op(arg1, arg2):
    return arg1 * arg2


def edge_embedding(model, edges_list, op):
    embedded_edges = []

    for edge in edges_list:
        node1_embedding = get_node_vector(model, edge[0])
        node2_embedding = get_node_vector(model, edge[1])

        embedded_edges.append(op(node1_embedding, node2_embedding))

    return embedded_edges
