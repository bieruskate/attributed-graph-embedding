"""
Reader functions
"""
import os

import networkx as nx
import numpy as np


def read_multi_graph_from_nx_edgelist(path, delimiter=' ', timestamp=True, nb_labels=0):
    kwargs = {
        'delimiter': delimiter,
        'create_using': nx.MultiGraph(),
        'nodetype': int,
    }

    data = []

    if timestamp:
        data.append(('timestamp', float))

    if nb_labels > 0:
        data.extend([(str(label), float) for label in np.arange(nb_labels)])

    if len(data) > 0:
        kwargs['data'] = data

    return nx.read_edgelist(path, **kwargs)


def read_edgelist(path):
    edge_list = []
    with open(path, 'r') as f:
        for line in f.readlines():
            edge_list.append(line.strip().split(' '))

    return edge_list


def read_ylist(path):
    y_list = []
    with open(path, 'r') as f:
        for line in f.readlines():
            y_list.append(int(line.strip()))

    return y_list


def read_dataset(path):
    x_train = read_edgelist(os.path.join(path, 'train.txt'))
    x_dev = read_edgelist(os.path.join(path, 'dev.txt'))
    y_train = read_ylist(os.path.join(path, 'y_train.txt'))
    y_dev = read_ylist(os.path.join(path, 'y_dev.txt'))

    return x_train, y_train, x_dev, y_dev
