import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


class SemiSkipGram(nn.Module):
    def __init__(self, emb_dim, label_dim, use_cuda,
                 edge_emb_op='concat', ml_loss=nn.BCEWithLogitsLoss):
        super(SemiSkipGram, self).__init__()
        self.edge_emb_op = edge_emb_op

        self.edge_embedding = {
            'concat': lambda u, v: torch.cat((u, v), dim=1),
            'hadamard': lambda u, v: torch.mul(u, v),
            'average': lambda u, v: (u + v) / 2
        }.get(edge_emb_op, None)

        if not self.edge_embedding:
            raise ValueError('undefined edge embedding operator')

        linear_scale = 2 if edge_emb_op == 'concat' else 1

        self.use_cuda = use_cuda
        self.ranking = nn.Sequential(
            nn.Linear(emb_dim * linear_scale, label_dim))
        self.criteria = ml_loss(size_average=False)

    @staticmethod
    def stucture_loss(emb_u, emb_v, neg_emb_v):
        score = torch.mul(emb_u, emb_v).squeeze()
        score = torch.sum(score, dim=1)
        score = F.logsigmoid(score)
        neg_score = torch.bmm(neg_emb_v, emb_u.unsqueeze(2)).squeeze().view(-1)
        neg_score = F.logsigmoid(-1 * neg_score)
        return -1 * (torch.sum(score) + torch.sum(neg_score))

    def edge_label_loss(self, emb_u, emb_v, labels):
        embeddings = self.edge_embedding(emb_u, emb_v)

        ranking = self.ranking(embeddings)
        loss = self.criteria(ranking, labels)
        return loss

    def combined_loss(self, structure_loss_params, edge_label_loss_params,
                      lambda_mul):
        structure_loss = self.stucture_loss(*structure_loss_params)
        edge_loss = self.edge_label_loss(*edge_label_loss_params)
        return lambda_mul * structure_loss + (1 - lambda_mul) * edge_loss

    def predicate_edge_label(self, emb_u, emb_v):
        embeddings = torch.cat((emb_u, emb_v), dim=1)
        ranking = self.ranking(embeddings)
        return ranking


class NodeRepresentation(nn.Module):
    def __init__(self, emb_size, emb_dim, use_cuda):
        super(NodeRepresentation, self).__init__()
        self.emb_size = emb_size
        self.emb_dim = emb_dim
        self.u_embeddings = nn.Embedding(emb_size, emb_dim, sparse=True)
        self.v_embeddings = nn.Embedding(emb_size, emb_dim, sparse=True)
        self.init_emb()
        self.use_cuda = use_cuda
        if use_cuda:
            self.cuda()

    def init_node_embedding(self, node_embs):
        if self.use_cuda:
            self.v_embeddings.weight.data = torch.from_numpy(
                node_embs[:, self.emb_dim:]
            ).cuda()
        else:
            self.u_embeddings.weight.data = torch.from_numpy(
                node_embs[:, :self.emb_dim]
            )
            self.v_embeddings.weight.data = torch.from_numpy(
                node_embs[:, self.emb_dim:]
            )

    def init_emb(self):
        initrange = 0.5 / self.emb_dim
        self.u_embeddings.weight.data.uniform_(-initrange, initrange)
        self.v_embeddings.weight.data.uniform_(-0, 0)

    def forward(self, nids, is_start, directed):
        if self.use_cuda:
            nids = Variable(torch.cuda.LongTensor(nids))
        else:
            nids = Variable(torch.LongTensor(nids))
        if directed:
            if is_start:
                return self.u_embeddings(nids)
            else:
                return self.v_embeddings(nids)
        else:
            return torch.cat(
                (self.u_embeddings(nids), self.v_embeddings(nids)), dim=1
            )

    def save_embedding(self, node_map, file_name, use_cuda):
        embeddings = self.u_embeddings.weight
        embeddings = embeddings.cpu().data.numpy()
        fout = open(file_name, 'w')
        fout.write("%d %d\n" % (self.emb_size, self.emb_dim))
        for wid in range(self.emb_size):
            e = embeddings[wid]
            w = node_map.id2str(wid)
            e = ' '.join(map(lambda x: str(x), e))
            fout.write('%s %s\n' % (w, e))
